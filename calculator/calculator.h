/*
@file: calculator.h
@author: ZZH
@date: 2022-05-05
@info: 计算组件
*/
#pragma once
#include "log.h"
#include "calculatorConf.h"

class ASTExpress_t;

class Calculator_t
{
private:
    using BasicType = CalculatorConf::BasicType;

    unsigned int totolPoint;
    BasicType fs;
    BasicType* pListOfT;

    //计算器配置数据
    CalculatorConf conf;

    Calculator_t():totolPoint(0), fs(0), pListOfT(nullptr)
    {
        COMP_INFO("Init");
    }

    ~Calculator_t()
    {
        COMP_INFO("Destroy");
    }

protected:
    bool allocArgs(void);
    void cleanArgs(void);
    BasicType* getPT(void) { return this->pListOfT; }

public:

    friend class ASTFunctionCall_t;

    static Calculator_t& getInst(void)
    {
        static Calculator_t inst;

        return inst;
    }

    //获取运算时需要分配的内存大小, 目前按照计算点数*最大数据类型分配
    inline size_t getMemSizeToAlloc(void) const { return this->getTotolPoint() * this->conf.getBiggestTypeSize(); }
    //获取计算点数
    inline unsigned int getTotolPoint(void) const { return this->totolPoint; }
    //设置计算点数
    inline void setTotolPoint(const unsigned int newValue) { this->totolPoint = newValue; }

    //获取采样率
    inline BasicType getFS(void) const { return this->fs; }
    //设置采样率
    inline void setFS(const BasicType newValue) { this->fs = newValue; }

    bool calculate(ASTExpress_t* exp, BasicType* pRes);

    //获取事先计算好的变量t向量
    inline BasicType getT(int index) const { return this->pListOfT[index]; }
};
