/*
@file: calculatorConf.cpp
@author: ZZH
@date: 2023-02-22
@info: 语法树计算器配置项
*/
#include "calculatorConf.h"
#include "fft.h"

unsigned int CalculatorConf::sizeOfBasicType = sizeof(CalculatorConf::BasicType);
unsigned int CalculatorConf::sizeOfBiggestType = sizeof(Complex_t);
unsigned int CalculatorConf::sizeFactor = CalculatorConf::sizeOfBiggestType / CalculatorConf::sizeOfBasicType;
