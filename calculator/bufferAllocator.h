/*
@file: bufferAllocator.h
@author: ZZH
@date: 2023-02-23
@info: buffer分配器
*/
#pragma once

#include "calculatorConf.h"
#include <cstdint>
#include <cstdlib>

class BufferAllocator
{
private:
    static CalculatorConf conf;
    static const char* messageFormat;
protected:

public:
    BufferAllocator() {}
    ~BufferAllocator() {}

    static void* getRawBuffer(const size_t size);

    template<typename T>
    static inline T* getBuffer(const size_t size)
    {
        return reinterpret_cast<T*>(getRawBuffer(size));
    }

    static inline void freeBuffer(void* buf) { free(buf); }
};
