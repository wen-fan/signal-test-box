#include "InnerLib.h"
#include <QDir>
#include <QMessageBox>

EXPORT void fs(pFunCallArg_t pArgs, BASIC_TYPE* output)
{
    char* arg0 = (char*) *(size_t*) pArgs->args[0];

    QDir dir(arg0);
    dir.setFilter(QDir::NoDotAndDotDot | QDir::AllEntries);

    if (not dir.exists())
    {
        COMP_INFO("%s is not exist", arg0);
        return;
    }

    COMP_INFO("list all entry of: %s", arg0);
    for (auto& str : dir.entryList())
    {
        COMP_INFO("%s", str.toStdString().c_str());
    }
    COMP_INFO("done");
}

EXPORT void cake(pFunCallArg_t pArgs, BASIC_TYPE* output)
{
    QMessageBox::information(nullptr, QObject::tr("cake"), QObject::tr("cake is a lie !!!"));
}
