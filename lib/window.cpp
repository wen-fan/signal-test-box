/*
@file: window.cpp
@author: ZZH
@date: 2023-02-03
@info: 窗函数库
*/
#include "libFun.h"

EXPORT void __hamming(pFunCallArg_t pArgs, BasicType* output)
{
    int allCalNum = pArgs->allCalNum;
    int len = (int) *static_cast<BasicType*>(pArgs->args[0]);//窗长度

    if (len > allCalNum || len < 2)
        return;

    hamming(output, len);
}


LibFunction_t funcs[] = {
    LIB_FUNCTION(__hamming, 1, .name = "hamming"),
    END_OF_LIB
};

register_function_lib(funcs);
