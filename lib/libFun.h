#pragma once
#include <cmath>
#include <cstdlib>
#include <ctime>
#include <algorithm>
#include "log.h"
#include "common/libCommon.h"

#define EXPORT extern "C"

typedef struct
{
    unsigned int allCalNum;//计算点数
    BasicType fs;//采样率
    BasicType* t;//预先计算好的每时刻的t
    void** args;//实际参数列表
}FunCallArg_t, * pFunCallArg_t;

typedef struct
{
    unsigned int argNum;//参数数量
    const char* sym;//实际函数名
    const char* name;//导入之后的函数名
}LibFunction_t, * pLibFunction_t;

//用于将一个函数添加到函数注册表中, 如果需要导入到符号表内的函数名和实际函数名不同, 可以使用.name指定符号表内的函数名
#define LIB_FUNCTION(fn, argn, ...) {.argNum = argn, .sym = #fn, ##__VA_ARGS__}
#define END_OF_LIB {.argNum = 0, .sym = 0, .name = 0}

//如果函数库在载入和退出时不需要做什么操作, 则可以使用此宏导出已有的函数注册表, 不用再次定义初始化函数
#define register_function_lib(func_reg_table)\
EXPORT pLibFunction_t lib_init(void){return func_reg_table;}
