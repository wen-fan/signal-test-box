/*
@file: SignalItem.cpp
@author: ZZH
@date: 2022-05-09
@info: 信号元素
*/
#include "SignalItem.h"
#include "compiler.h"

const QList<QPair<QString, char>> SignalItem::replaceTable =
{
    {"−", '-'},
    {"，", ','},
    {"。", '.'},
    {"；", ';'},
    {"“", '"'},
    {"”", '"'},
    {"？", '?'},
    {"：", ':'},
};

SignalItem::SignalItem(const QString& name): QListWidgetItem(name)
{
    UI_INFO("New item: %s", name.toStdString().c_str());

    this->compileRes = nullptr;
    this->dirty = true;
    this->isFFTMode = false;

    this->setFlags(this->flags() | Qt::ItemIsEditable);
}

bool SignalItem::compile(void)
{
    if (true == this->isDirty())
    {
        COMP_INFO("Compile dirty signal %s", this->text().toStdString().c_str());

        if (nullptr != this->compileRes)
            delete this->compileRes;

        this->compileRes = Compiler_t::getInst().compile(this->sourceCode);

        if (nullptr == this->compileRes)
            return false;

        this->dirty = false;
    }

    COMP_INFO("Signal %s is clean", this->text().toStdString().c_str());
    return true;
}

QString SignalItem::replaceNonStdChar(QString code)
{
    for (const auto& replacePair : SignalItem::replaceTable)
        code.replace(replacePair.first, QString(replacePair.second));

    return code;
}
