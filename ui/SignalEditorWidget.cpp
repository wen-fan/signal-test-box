/*
@file: SignalEditor.cpp
@author: ZZH
@date: 2022-10-31
@info: 信号编辑器
*/
#include "SignalEditorWidget.h"

void SignalEditorWidget::setUp()
{
    this->pTextEdit = this->findChild<SignalEditor*>("pSignalExpress");
    this->pTextEdit->setAcceptDrops(true);

    this->pCodeTip = new CodeTip(this->pTextEdit, this);
    this->pCodeTip->setObjectName(QString::fromUtf8("pCodeTip"));
    this->pCodeTip->setGeometry({ 0, 0, 300, 200 });
    this->pCodeTip->raise();

    this->pCodeTipTimer = new QTimer(this);
    this->pCodeTipTimer->setSingleShot(true);
    this->pCodeTipTimer->stop();

    connect(this->pTextEdit, &QTextEdit::textChanged, this, [this]() {this->pCodeTipTimer->start(500); this->pCodeTip->hide();});
    connect(this->pCodeTipTimer, &QTimer::timeout, this, &SignalEditorWidget::requestCodeTip);
}

void SignalEditorWidget::requestCodeTip(void)
{
    const auto& rect = this->pTextEdit->cursorRect();
    auto column = this->pTextEdit->textCursor().position();
    const auto& localPoint = this->pTextEdit->mapTo(this, QPoint(rect.x(), rect.y() + rect.height()));
    this->pCodeTip->requestCodeTip(localPoint, this->pTextEdit->toPlainText().left(column));
}

void SignalEditorWidget::keyPressEvent(QKeyEvent* e)
{
    this->pTextEdit->keyPressEvent(e);
}
